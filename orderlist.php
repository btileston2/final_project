<!doctype html>
<?php
require('mlib_functions.php');
require('mlib_values.php');
html_head("Cake orders");
require('mlib_header.php');
session_start();
require('cake_sidebar.php');

# Code for your web page follows.
try
{
  //open database
  $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
?>
 
<h2>Orders</h2>
 <!-- Display media -->
    <table border=1>
      <tr>
        <td>Size</td><td>Flavor</td><td>Frosting</td><td>Icecream</td><td>Pickup Date</td><td>Customer</td>
      </tr>

<?php
  $query = "SELECT * FROM cake";
  $result = $db->query($query);
  foreach($result as $row) {
    print "<tr>";
    print "<td>".$row['size']."</td>";
    print "<td>".$row['flavor']."</td>";
    print "<td>".$row['frosting']."</td>";
    print "<td>".$row['icecream']."</td>";
    $date_in = $row['date_by'];
    print "<td>".$date_in."</td>";
    print "<td>".$row['c_id']."</td>";
    print "</tr>";
  }

 print "</table>";

 // close connection
 $db = NULL;
}
catch(PDOExeption $e)
{
  echo 'Exception : '.$e->getMessage();
  echo "<br/>";
  $db = NULL;
}
require('mlib_footer.php');
?>
