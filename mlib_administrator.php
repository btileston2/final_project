<!doctype html>
<?php
require('mlib_functions.php');
require('mlib_values.php');
html_head("Admin Page");
require('mlib_header.php');
session_start();
require('mlib_sidebar.php');

if (we_are_not_admin()) {
  exit;
}

# Code for your web page follows.
if (!isset($_POST['submit']))
{

  try
  {
    //open db
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
?>
  <!-- Display a form to capture information -->
  <h2>Change Admin Priviledges</h2>
  <form action="mlib_administrator.php" method="post">
    <table border=1>
      <tr>
        <td>Click to Change</td><td>User</td><td>Login</td>
      </tr>
<?php

    $result = $db->query("SELECT * FROM mlib_users");
    foreach($result as $row)
    {
      print "<tr>";
      print "<td><input type='radio' name='id' value=".$row['id']."></td>";
      print "<td>".$row['first']." ".$row['last']."</td>";
      print "<td>".$row['login']."</td>";
      print "</tr>";
    }
?>
    </table>
    <p>Clicking an entry with a login will remove admin status.</p>
    <p>Clicking an entry w/o a login will enable admin status. Enter login and password.</p>
    Login: <input type="text" name="login"/><br/>
    Password: <input type="text" name="password"/><br/>
    <input type="submit" name="submit" value = "Submit"/><br/>
  </form>


<?php

    //close
    $db = NULL;
  }
  catch(PDOException $e)
  {
    echo 'Exception : '.$e->getMessage().'<br/>';
    $db = NULL;
  }
} else {
?>

  <h2>Administration Priviledges Changed</h2>

<?php
  $id = $_POST['id'];
  $login = $_POST['login'];
  $password = $_POST['password'];

  try
  {
    if (empty($id)) { 
      echo "You did not select any users to change status.<br/>";
    } else {
      //open db
      $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
      $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      //update user as appropriate
      $sql = "SELECT * FROM mlib_users WHERE id = $id";
      $result = $db->query($sql)->fetch(PDO::FETCH_ASSOC);

      if (empty($result['login'])) {
        //set login and password
        //clean and validate data
        $login = trim($login);
        if ( empty($login) ) {
          try_again("Login cannot be empty.");
        }
        $password = trim($password);
        if ( strlen($password)< 8 ) {
          try_again("Password must be at least 8 charas.");
        }
        //change password into a sha1 hash
        $passwd = sha1($password);
        //check for duplicate
        $sql = "select count(*) from mlib_users where login = '$login'";
        $result = $db->query($sql)->fetch();//count num entrees w/name
        if ( $result[0] > 0) {
          try_again($login." is not unique. Logins must be unique.");
        }
        $db->exec("UPDATE mlib_users SET login = '$login', password = '$passwd' WHERE id = $id");
      } else {
       //rm login and password
       $db->exec("UPDATE mlib_users SET login = NULL, password = NULL WHERE id = $id");
      }

      //now output to a table
      print "<table border=1>";
      print "<tr>";
      print "<td>User</td><td>Login</td>";
      print "</tr>";
      $sql = "SELECT * FROM mlib_users where id = $id";
      $result = $db->query($sql)->fetch(PDO::FETCH_ASSOC);
      print "<tr>";
      print "<td>".$result['first']." ".$result['last']."</td><td>".$result['login']."</td>";
      print "</tr>";
      print "</table>";
    }
    //close db
    $db = NULL;
  }
  catch(PDOException $e)
  {
    echo 'Exception : '.$e->getMessage().'<br/>';
    $db = NULL;
  }
}
require('mlib_footer.php');
?>
