<!doctype html>
<?php
require('mlib_functions.php');
require('mlib_values.php');
html_head("Cake Order");
require('mlib_header.php');
session_start();
require('cake_sidebar.php');

//if (we_are_not_admin()) {
  //exit;
//}

# Code for your web page follows.
if (!isset($_POST['submit']))
{
?>
  <!-- Display a form to capture information -->
  <h2>Choose what you want</h2>
  <form action="cake_order.php" method="post">
    <table border="0">
      <tr bgcolor="#cccccc">
        <td width="100">Field</td>
        <td width="300">Value</td>
      <tr>
        <td>Size</td>
        <td align="left">
		   <select name="size">
<?php
  //select pull down menu
  try
  {
    //open db
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //display from mlib_types
    $result = $db->query('SELECT size FROM type');
    foreach($result as $row)
    {
      print "<option value=".$row['size'].">".$row['size']."</option>";
    }

    //close db
    $db = NULL;
  }

  catch(PDOException $e)
  {
    echo 'Exception : '.$e->getMessage();
    echo "<br/>";
    $db = NULL;
  }
?>
	</select>
       </td>
      </tr>
      <tr>
        <td>Flavor</td>
        <td align="left">
                   <select name="flavor">
<?php
  //select pull down menu
  try
  {
    //open db
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //display from mlib_types
    $result = $db->query('SELECT flavor FROM type');
    foreach($result as $row)
    {
      print "<option value=".$row['flavor'].">".$row['flavor']."</option>";
    }

    //close db
    $db = NULL;
  }

  catch(PDOException $e)
  {
    echo 'Exception : '.$e->getMessage();
    echo "<br/>";
    $db = NULL;
  }
?>
        </select>
       </td>
      </tr>
      <tr>
        <td>Frosting</td>
        <td align="left">
                   <select name="frosting">
<?php
  //select pull down menu
  try
  {
    //open db
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //display from mlib_types
    $result = $db->query('SELECT frosting FROM type');
    foreach($result as $row)
    {
      print "<option value=".$row['frosting'].">".$row['frosting']."</option>";
    }

    //close db
    $db = NULL;
  }

  catch(PDOException $e)
  {
    echo 'Exception : '.$e->getMessage();
    echo "<br/>";
    $db = NULL;
  }
?>
        </select>
       </td>
      </tr>
      <tr>
        <td>Icecream</td>
        <td align="left">
                   <select name="icecream">
<?php
  //select pull down menu
  try
  {
    //open db
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //display from mlib_types
    $result = $db->query('SELECT icecream FROM type');
    foreach($result as $row)
    {
      print "<option value=".$row['icecream'].">".$row['icecream']."</option>";
    }

    //close db
    $db = NULL;
  }

  catch(PDOException $e)
  {
    echo 'Exception : '.$e->getMessage();
    echo "<br/>";
    $db = NULL;
  }
?>
        </select>
       </td>
      </tr>
      <tr>
        <td>Customer</td>
        <td align="left">
                   <select name="last">
<?php
  //select pull down menu
  try
  {
    //open db
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //display from mlib_types
    $result = $db->query('SELECT last FROM customers');
    foreach($result as $row)
    {
      print "<option value=".$row['last'].">".$row['last']."</option>";
    }

    //close db
    $db = NULL;
  }
  catch(PDOException $e)
  {
    echo 'Exception : '.$e->getMessage();
    echo "<br/>";
    $db = NULL;
  }
?>
        </select>
       </td>
      </tr>
      <tr>
        <td colspan="2" align="right"><input type="submit" name="submit" value="Submit"></td>
      </tr>
    </table>
  </form>
<?php
} else {
  # Process the information from the form displayed
  $size = $_POST['size'];
  $flavor = $_POST['flavor'];
  $frosting = $_POST['frosting'];
  $icecream = $_POST['icecream'];
  $last = $_POST['last'];


  //clean up data
  if ( empty($size) ) {
    try_again("size field is required.");
  }
  if ( empty($flavor) ) {
    try_again("flavor field is required.");
  }
  if ( empty($frosting) ) {
    try_again("frosting field is required.");
  }
  if ( empty($icecream) ) {
    try_again("icecream field is required.");
  }
  if ( empty($last) ) {
    try_again("Please choose your name. If it's not there please add your information in the New customers page.");
  }

  try
  {
    //open database
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $result = $db->query("SELECT * FROM customers where last = '$last'")->fetch();
    $customer_id = $result['id'];

    //insert data
    $db->exec("INSERT INTO cake (size, flavor, frosting, icecream, date_by, c_id) VALUES ('$size', '$flavor', '$frosting', '$icecream', '2019-12-22', $customer_id);");

    $last_id = $db->lastInsertId();
    //now output html table
    print "<h2>Cake Order</h2>";
    print "<table border=1>";
    print "<tr>";
    print "<td>Id</td><td>Size</td><td>Flavor</td><td>Frosting</td><td>Icecream</td><td>Order Date</td>";
    print "</tr>";
    $row = $db->query("SELECT * FROM cake where id = '$last_id'")->fetch(PDO::FETCH_ASSOC);
    print "<tr>";
    print "<td>".$row['id']."</td>";
    print "<td>".$row['size']."</td>";
    print "<td>".$row['flavor']."</td>";
    print "<td>".$row['frosting']."</td>";
    print "<td>".$row['icecream']."</td>";
    print "<td>".$row['date_by']."</td>";
    print "</tr>";
    print "</table>";

    //close
    $db = NULL;
  }
  catch(PDOException $e)
  {
    echo 'Exception : '.$e->getMessage();
    echo "<br/>";
    $db = NULL;
  }
}
require('mlib_footer.php');
?>
