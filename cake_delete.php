<!doctype html>
<?php
require('mlib_functions.php');
require('mlib_values.php');
html_head("Cake orders");
require('mlib_header.php');
session_start();
require('cake_sidebar.php');

# Code for your web page follows.

if (!isset($_POST['submit']))
{

 try
 {
  //open database
  $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
?>
 
<h2>Delete Orders</h2>
 <form action="cake_delete.php" method="post">
    <table border=1>
      <tr>
        <td>Click one to Change</td><td>Size</td><td>Flavor</td><td>Frosting</td><td>Icecream</td><td>Customer</td><td>Pickup Date</td>
      </tr>

<?php
  $query = "SELECT * FROM cake";
  $result = $db->query($query);
  foreach($result as $row) {
    print "<tr>";
    print "<td><input type='radio' name='id' value=".$row['id']."></td>";
    print "<td>".$row['size']."</td>";
    print "<td>".$row['flavor']."</td>";
    print "<td>".$row['frosting']."</td>";
    print "<td>".$row['icecream']."</td>";
    $result = $db->query("SELECT * FROM customers")->fetch();
    $user_name = $result['first']." ".$result['last'];
    $date_in = $row['date_by'];
    print "<td>".$user_name."</td>";
    print "<td>".$date_in."</td>";
    print "</tr>";
  }

 print "</table>";
 print "<input type='submit' name='submit' value='Submit'/><br/>";
 print "</form>";

 // close connection
 $db = NULL;
}
catch(PDOExeption $e)
{
  echo 'Exception : '.$e->getMessage();
  echo "<br/>";
  $db = NULL;
}
} else {
?>

  <h2>Order deleted</h2>

<?php
  $id = $_POST['id'];

  try
  {
    if (empty($id)) {
      echo "You did not select any orders to delete.<br/>";
    }

  //open database
  $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  $db->exec("DELETE from cake where id = $id");
  }

 // close connection
 $db = NULL;
}
catch(PDOExeption $e)
{
  echo 'Exception : '.$e->getMessage();
  echo "<br/>";
  $db = NULL;
}

require('mlib_footer.php');
?>
