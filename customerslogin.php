<!doctype html>
<?php
require('mlib_functions.php');
require('mlib_values.php');
html_head("Add Customers");
require('mlib_header.php');
session_start();
require('cake_sidebar.php');


# Code for your web page follows.
if (!isset($_POST['submit']))
{
?>
  <!-- Display a form to capture information -->
  <h2>Add Customers</h2>
  <form action="customers.php" method="post">
    <table border="0">
      <tr bgcolor="#cccccc">
        <td width="100">Field</td>
        <td width="300">Value</td>
      </tr>
      <tr>
        <td>First Name</td>
        <td align="left"><input type="text" name="first" size="35" maxlength="35"></td>
      </tr>
      <tr>
        <td>Last Name</td>
        <td align="left"><input type="text" name="last" size="35" maxlength="35"></td>
      </tr>
      <tr>
        <td>Email</td>
        <td align="left"><input type="text" name="email" size="35" maxlength="35"></td>
      </tr>
      <tr>
        <td>Username</td>
        <td align="left"><input type="text" name="login" size="35" maxlength="35"></td>
      </tr>
      <tr>
        <td>Password</td>
        <td align="left"><input type="password" name="password" size="35" maxlength="35"></td>
      </tr>
      <tr>
        <td colspan="2" align="right"><input type="submit" name="submit" value="Submit"></td>
      </tr>
    </table>
  </form>
<?php
} else {
  # Process the information from the form displayed
  $first = $_POST['first'];
  $last = $_POST['last'];
  $email = $_POST['email'];
  $login = $_POST['login'];
  $password = $_POST['password'];

  //clean up data
  $first = trim($first);
  if ( empty($first) ) {
    try_again("First Name is required.");
  }
  $last = trim($last);
  if ( empty($last) ) {
    try_again("Last Name is required.");
  }
  $email = trim($email);
  if ( empty($email) ) {
    try_again("Email is required.");
  }
  $login = trim($login);
  if ( empty($login) ) {
    try_again("Login is required.");
  }
  $password = trim($password);
  if ( strlen($password)< 8  ) {
    try_again("Password must be at least 8 characters.");
  }

  //make password into a sha1 hash
  $passwrd = sha1($password);

  try
  {
    //open database
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //check for duplicate
    $sql = "select count(*) from customers where login = '$login'";
    $result = $db->query($sql)->fetch();

    if ( $result[0] > 0) {
      try_again($login." is not unique. Logins must be unique.");
    }
    //insert data
    $db->exec("INSERT INTO customers (first, last, email, login, password) VALUES ('$first', '$last', '$email', '$login', '$passwrd');");

    $last_id = $db->lastInsertId();
    //now output html table
    print "<h2>New Customer</h2>";
    print "<table border=1>";
    print "<tr>";
    print "<td>Id</td><td>Name</td><td>Email</td><td>Username</td>";
    print "</tr>";
    $row = $db->query("SELECT * FROM customers where id = '$last_id'")->fetch(PDO::FETCH_ASSOC);
    print "<tr>";
    print "<td>".$row['id']."</td>";
    print "<td>".$row['first']." ".$row['last']."</td>";
    print "<td>".$row['email']."</td>";
    print "<td>".$row['login']."</td>";
    print "</tr>";
    print "</table>";

    //close
    $db = NULL;
  }
  catch(PDOException $e)
  {
    echo 'Exception : '.$e->getMessage();
    echo "<br/>";
    $db = NULL;
  }
}
require('mlib_footer.php');

?>
