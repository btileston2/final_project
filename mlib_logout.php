<!doctype html>
<?php
session_start();
require('mlib_functions.php');
require('mlib_values.php');
html_head("Logout");
require('mlib_header.php');


//save sess to see if we wer logged in to begin with
$old_user = $_SESSION['valid_user'];
unset($_SESSION['valid_user']);
session_destroy();

require('mlib_sidebar.php');
print "<h2>Log out</h2>";

if (empty($old_user)) {
  print "You were not logged in to begin with.<br/>";
} else {
  print "Logged out<br/>";
}

require('mlib_footer.php');
?>
