<!doctype html>
<?php
require('mlib_functions.php');
require('mlib_values.php');
html_head("Delete Order");
require('mlib_header.php');
session_start();
require('cake_sidebar.php');

//if (we_are_not_admin()) {
  //exit;
//}

# Code for your web page follows.
if (!isset($_POST['submit']))
{
?>
  <!-- Display a form to capture information -->
  <h2>Chose Name to Delete Order</h2>
  <form action="deleteorder.php" method="post">
    <table border="0">
      <tr bgcolor="#cccccc">
        <td width="100">Field</td>
        <td width="300">Value</td>
      </tr>
      <tr>
        <td>Customer Last Name</td>
        <td align="left">
                   <select name="last">
<?php
  try
  {
    //open db
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $result = $db->query("SELECT * FROM cake");
    foreach($result as $row)
    {
      print "<option value=".$row['last'].">".$row['last']."</option>";
    }

    //close
    $db = NULL;
  }
  catch(PDOException $e)
  {
    echo 'Exception : '.$e->getMessage();
    echo "<br/>";
    $db = NULL;
  }
?>
      </select>
      </td>
      </tr>
      <tr>
        <td colspan="2" align="right"><input type="submit" name="submit" value="Submit"></td>
      </tr>
    </table>
  </form>
<?php
} else {

  # Process the information from the form displayed
  $last = $_POST['last'];

  try
  {
    //open database
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $result = $db->query("SELECT cake.c_id, customers.id FROM cake inner join customers ON customers.id = ");

    $db->exec("DELETE FROM cake where c_id = $result[0]");
    //now output html table
    print "<p>Order deleted</p>";

    //close
    $db = NULL;
  }
  catch(PDOException $e)
  {
    echo 'Exception : '.$e->getMessage();
    echo "<br/>";
    $db = NULL;
  }
}
require('mlib_footer.php');
?>
