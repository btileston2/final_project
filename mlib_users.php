<!doctype html>
<?php
require('mlib_functions.php');
require('mlib_values.php');
html_head("Add Users");
require('mlib_header.php');
session_start();
require('mlib_sidebar.php');

if (we_are_not_admin()) {
  exit;
}

# Code for your web page follows.
if (!isset($_POST['submit']))
{
?>
  <!-- Display a form to capture information -->
  <h2>Add Media</h2>
  <form action="mlib_users.php" method="post">
    <table border="0">
      <tr bgcolor="#cccccc">
        <td width="100">Field</td>
        <td width="300">Value</td>
      </tr>
      <tr>
        <td>First</td>
        <td align="left"><input type="text" name="first" size="35" maxlength="35"></td>
      </tr>
      <tr>
        <td>Last</td>
        <td align="left"><input type="text" name="last" size="35" maxlength="35"></td>
      </tr>
      <tr>
        <td>Email</td>
        <td align="left"><input type="text" name="email" size="35" maxlength="35"></td>
      </tr>
      <tr>
        <td colspan="2" align="right"><input type="submit" name="submit" value="Submit"></td>
      </tr>
    </table>
  </form>
<?php
} else {

  # Process the information from the form displayed

  $first = $_POST['first'];
  $last = $_POST['last'];
  $email = $_POST['email'];

  try
  {
    //open database
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //clean up data
    $first = trim($first);
    if ( empty($first) ) {
      try_again("First name is required.");
    }
    $last = trim($last);
    if ( empty($last) ) {
    try_again("Last field must have a name.");
    }
    $email = trim($email);
    if ( empty($email) ) {
    try_again("Email field is required.");
    }

    //check duplicate user name
    $sql = "SELECT COUNT(*) FROM mlib_users WHERE first = '$first' AND last = '$last'";
    $result = $db->query($sql)->fetch(); //count entries w name
    if ( $result[0] > 0) {
      try_again($first." ".$last." is not unique. Names must be unique.");
    }

    //check for duplicate email
    $sql = "SELECT COUNT(*) FROM mlib_users where email = '$email'";
    $result = $db->query($sql)->fetch(); //count entries w email
    if ( $result[0] > 0) {
      try_again($email." is not unique. Email must be unique.");
    }

    //insert data
    $db->exec("INSERT INTO mlib_users (first, last, email) VALUES ('$first', '$last', '$email');");

    //get last id val
    $last_id = $db->lastInsertId();

    //now output html table
    print "<h2>User Added</h2>";
    print "<table border=1>";
    print "<tr>";
    print "<td>Id</td><td>First</td><td>Last</td><td>Email</td>";
    print "</tr>";
    $row = $db->query("SELECT * FROM mlib_users where id = '$last_id'")->fetch(PDO::FETCH_ASSOC);
    print "<tr>";
    print "<td>".$row['id']."</td>";
    print "<td>".$row['first']."</td>";
    print "<td>".$row['last']."</td>";
    print "<td>".$row['email']."</td>";
    print "</tr>";
    print "</table>";

    //close
    $db = NULL;
  }
  catch(PDOException $e)
  {
    echo 'Exception : '.$e->getMessage();
    echo "<br/>";
    $db = NULL;
  }
}
require('mlib_footer.php');
?>
